<!DOCTYPE html>
<html lang="en">
<head> 

  <link href="css/bootstrap.min.css" rel="stylesheet" media="screen"> 
  <meta name="viewport" content="width=device-width, initial-scale=1"> 

  <script src="js/jquery.min.js"></script> 
  <!--<script src="js/popper.min.js"></script>--> 
  <script src="js/bootstrap.min.js"></script>

</head>
<body>

  <div class="container">
    <header class="text-center">
      <h1>Control de asistencia</h1>
    </header>
    <div class="col col-md-12 col-sm-12 col-12">
      <p>Ingrese sus datos</p>
      <p></p>
      <form class="md-form">
        <div class="form-group">
          <label for="nombre"> Nombre: </label>
          <input id="nombre" class="form-control" placeholder="Ingresa tu nombre">
          <small class="form-text text-muted">Ingresa el mismo nombre que existe en la base de datos</small>
        </div>
        <div class="form-group">
          <label for="passwd">Password</label>
          <input type="password" class="form-control" id="passwd" placeholder="Ingresa tu contraseña">
        </div>
        <div class="form-group">
          <label for="hora">Hora actual</label>
          <?php $d=strtotime("-6 Hours"); ?>
            <input type="time" id="hora" class="form-control" value=<?php echo date("H:i", $d);?> disabled>
        </div>
        <button type="submit" class="btn btn-primary"> Enviar </button>
      </form>
    </div>
  </div>
</body>
</html>